#include <GLFW/glfw3.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>

#define PI 3.14159265359

// ################ STRUCTS ##############

typedef struct { GLdouble x, y, z; } VECTOR3;
typedef struct { GLdouble x, y, z, w; } VECTOR4;
typedef struct { GLint v[4];  } FACE;
typedef GLdouble MATRIX4[4][4];

// ################ GLOBAL VARIABLES ##############

std::vector<VECTOR4> pointsOfTorus; // contains all the points of the torus
std::vector<FACE> faces; // contains all faces of the torus

double rotating = 0.0; // the rotate matrix alpha
double upup = 0.0; // the eye.y move step
double zoom = 37.0;

GLdouble winWidth = 800.0f;
GLdouble winHeight = 600.0f;

GLdouble alpha = 0.0f;
GLdouble deltaAlpha = (PI / 180.0) * 3;

MATRIX4 view; 	// camera
MATRIX4 Vc; // central projection
MATRIX4 Wc; // window to viewport transf.

MATRIX4 Wc_Vc; // wtv * central projection
MATRIX4 rotateX; // rotate around x axis

VECTOR3 eye, up, centerVec; // camera vectors

int cStep = 39; // the step of the little circles ( PI / 39 )
int pStep = 10; // the step of the points of one little circle ( PI / 10 )

GLclampd center = 6.0f; // center of the central projection

// ################ FUNCTION DECLARATIONS ##############

VECTOR3 initVector3( GLdouble, GLdouble, GLdouble );
VECTOR4 initVector4( GLdouble, GLdouble, GLdouble, GLdouble );
FACE initFace( GLint, GLint, GLint, GLint );
VECTOR3 convertToInhomogen( VECTOR4 );
VECTOR4 convertToHomogen( VECTOR3 );
GLdouble getVectorLength( VECTOR3 );
VECTOR3 normalizeVector( VECTOR3 );
VECTOR3 vecSub( VECTOR3, VECTOR3 );
GLdouble dotProduct( VECTOR3, VECTOR3 );
VECTOR3 crossProduct( VECTOR3, VECTOR3 );
void initIdentityMatrix( MATRIX4 );
void initPersProjMatrix( MATRIX4, GLdouble );
void initRotateMatrixX( MATRIX4, GLdouble );
void initMoveMatrix( MATRIX4, VECTOR3 );
void initScaleMatrix( MATRIX4, double, double, double );
void initWtvMatrix( MATRIX4, double, double, double, double,
														 double, double, double, double );
void initViewMatrix( MATRIX4, VECTOR3, VECTOR3, VECTOR3 );
VECTOR4 mulMatrixVector( MATRIX4, VECTOR4 );
void mulMatrices( MATRIX4, MATRIX4, MATRIX4 );
VECTOR3 getGravity( FACE );
bool compareFacesZ( FACE, FACE );
void init();
void initTransformations();
void keyPressed( GLFWwindow*, GLint, GLint, GLint, GLint );

// ############### TORUS CLASS ##############

class Torus
{
private:
	double R; // R is the distance from the center of the tube to the center of the torus,
	double r; // r is the radius of the tube.
public:

	Torus( double R, double r )
	{
		this->R = R;
		this->r = r;
	}

	void drawTorus( MATRIX4 T, MATRIX4 rotate, VECTOR3 color, MATRIX4 view )
	{
		// T = Wc * Vc

		glLineWidth(1.0f);

		VECTOR4 pt, ph;
		VECTOR3 pih;

		// the number little circles in the tours
		double circleStep = (2 * PI) / (PI / cStep) + 1;
		// the number of points on a little circle
		double pointStep = (2 * PI) / (PI / pStep) + 1; // also the point that has to be skipped

		// NOTE: the torus has circleStep * pointStep points

		MATRIX4 view_rotate;
		mulMatrices( view, rotate, view_rotate );
		// view_rotate = view * rotate

		// collect the vertices coordinates
		// small circles
		for( double phi = 0; phi <= 2*PI+0.001; phi += PI/cStep )
		{
			for( double theta = 0; theta <= 2*PI+0.001; theta += PI/pStep )
		  {
				ph = initVector4( (R+r*cos(theta))*cos(phi), r*sin(theta), (R+r*cos(theta))*sin(phi), 1.0 );

				pt = mulMatrixVector(view_rotate, ph);
				// view_rotate = view * rotate
				// store the homogen coordinates of the torus with the view and rotate in it
				pointsOfTorus.push_back( pt );
			}
		}
		// collect faces indexes
		int counter = 1;
		// go through all the points except the last circle's (also the first circle)
		// does not need -1 cuz the counter will continue when it reach the -1 part
		for( int i = 0; i < pointsOfTorus.size()-pointStep; i++ )
		{
			// if we reach the last point of a small circle
			// which is also the first point of that circle
			if( counter == pointStep )
			{
				counter = 1;
				// do not bother that point, but go to the next point
				// which is the first point of the following small circle
				continue;
			}
			// the actual point
			// the following point on that circle
			// the following point on that circle plus the number of point on a circle + 1 ( because of the starting and ending point )
			// the actual point plus the number of point on a circle + 1 (because of the starting and ending point )

			// EXAMPLE
			// if a circle has 20 points on it (starting with 0 and ending with 20 but these two is the same point )
			// the first face will consist of the 0, 1, 22, 21
			faces.push_back( initFace( i, i+1, i+1+pointStep, i+pointStep ) );
			counter++;
		}

		// sort the faces
		std::sort( faces.begin(), faces.end(), compareFacesZ );

		VECTOR3 normalV;
		VECTOR3 toCam;

		// calculate every face's normal vector
		for( int i = 0; i < faces.size(); ++i )
		{
			VECTOR3 edges[2]=
			{
				vecSub(convertToInhomogen(pointsOfTorus[faces[i].v[0]]),
							 convertToInhomogen(pointsOfTorus[faces[i].v[1]])),

				vecSub(convertToInhomogen(pointsOfTorus[faces[i].v[0]]),
							 convertToInhomogen(pointsOfTorus[faces[i].v[2]]))
			};

			normalV = normalizeVector(crossProduct( edges[0], edges[1] ) );

			VECTOR3 average = getGravity( faces[i] );

			toCam = normalizeVector( vecSub( average, initVector3(0.0f, 0.0f, center) ) );

			// if we could see the face
			if( dotProduct( normalV, toCam ) > 0 )
			{
					glColor3f( color.x, color.y, color.z );
					glBegin(GL_POLYGON);
					for( int j = 0; j < 4; ++j )
					{
						pt = mulMatrixVector( T, pointsOfTorus[faces[i].v[j]] );
						// T = Wc * Vc
						pih = convertToInhomogen( pt );

						glVertex2f(pih.x, pih.y);
					}
					glEnd();

					glColor3f( 0.0, 0.0, 0.0 );
					glBegin(GL_LINE_STRIP);
					for( int j = 0; j < 4; ++j )
					{
						pt = mulMatrixVector( T, pointsOfTorus[faces[i].v[j]] );
						// T = Wc * Vc
						pih = convertToInhomogen( pt );

						glVertex2f(pih.x, pih.y);
					}
					glEnd();
			}
		}

		faces.clear();
		pointsOfTorus.clear();
	}
};

int main()
{
	GLFWwindow* window;

	// Initialize the library
	if( !glfwInit() )
		return -1;

	// Create a windowed mode window and its OpenGL context
	window = glfwCreateWindow(winWidth, winHeight, "Fourth TorusVisibility", NULL, NULL);
	if( !window )
	{
		glfwTerminate();
		return -1;
	}

	// Make the window's context current
	glfwMakeContextCurrent(window);

	glfwSetKeyCallback(window, keyPressed);

	init();

	Torus onlyOne = Torus(8.0, 5.0);

	// Loop until the user closes the window
	while( !glfwWindowShouldClose(window) )
	{
		glClear( GL_COLOR_BUFFER_BIT );

		initRotateMatrixX( rotateX, rotating );
    onlyOne.drawTorus( Wc_Vc, rotateX, initVector3( 0.0f, 1.0f, 0.0f), view );
		rotating -= 0.5;

		glfwSwapBuffers(window);

		glfwPollEvents();
	}

	glfwTerminate();

	return 0;
}

// ################ INITIALIZER AND OPERATING FUNCTIONS ##############

VECTOR3 initVector3( GLdouble x, GLdouble y, GLdouble z )
{
	VECTOR3 P;
	P.x = x;
	P.y = y;
	P.z = z;
	return P;
}
VECTOR4 initVector4( GLdouble x, GLdouble y, GLdouble z, GLdouble w )
{
	VECTOR4 P;
	P.x = x;
	P.y = y;
	P.z = z;
	P.w = w;
	return P;
}
FACE initFace( GLint v0, GLint v1, GLint v2, GLint v3 )
{
	FACE f;
	f.v[0] = v0;
	f.v[1] = v1;
	f.v[2] = v2;
	f.v[3] = v3;
	return f;
}
VECTOR3 convertToInhomogen( VECTOR4 vector )
{
	return initVector3( vector.x / vector.w, vector.y / vector.w, vector.z / vector.w );
}
VECTOR4 convertToHomogen( VECTOR3 vector )
{
	return initVector4( vector.x, vector.y, vector.z, 1.0 );
}
GLdouble getVectorLength( VECTOR3 vector )
{
	return sqrt(pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2));
}
VECTOR3 normalizeVector( VECTOR3 vector )
{
	GLdouble length = getVectorLength(vector);
	return initVector3(vector.x / length, vector.y / length, vector.z / length);
}
VECTOR3 vecSub( VECTOR3 a, VECTOR3 b )
{
	return initVector3( b.x - a.x, b.y - a.y, b.z - a.z);
}
GLdouble dotProduct( VECTOR3 a, VECTOR3 b )
{
	return (a.x * b.x + a.y * b.y + a.z * b.z);
}
VECTOR3 crossProduct( VECTOR3 a, VECTOR3 b )
{
	return initVector3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x );
}
void initIdentityMatrix( MATRIX4 A )
{
	for( int i = 0; i<4; i++)
		for( int j = 0; j<4; j++)
			A[i][j] = 0.0f;

	for( int i = 0; i<4; i++)
		A[i][i] = 1.0f;
}
void initPersProjMatrix( MATRIX4 A, GLdouble s )
{
	initIdentityMatrix(A);

	A[2][2] = 0.0f;
	A[3][2] = -1.0f / s;
}
void initRotateMatrixX( MATRIX4 A, GLdouble alpha )
{
	initIdentityMatrix(A);

	A[1][1] = cos( (PI / 180) * alpha );
	A[1][2] = -1.0 * sin( (PI / 180) * alpha );

	A[2][1] = sin( (PI / 180) * alpha );
	A[2][2] = cos( (PI / 180) * alpha );
}
void initMoveMatrix( MATRIX4 A, VECTOR3 d )
{
	initIdentityMatrix(A);
	A[0][3] = d.x;
	A[1][3] = d.y;
	A[2][3] = d.z;
}
void initScaleMatrix( MATRIX4 A, double lambda, double mu, double nu )
{
	initIdentityMatrix(A);
	A[0][0] = lambda;
	A[1][1] = mu;
	A[2][2] = nu;
}
void initWtvMatrix( MATRIX4 A, double vleft, double vbottom, double vright, double vtop,
															 double wleft, double wbottom, double wright, double wtop )
{
	initIdentityMatrix(A);

	// 0, 0, 600, 600, -3, -3, 3, 3

	A[0][0] = (vright-vleft)/(wright-wleft);
	A[1][1] = (vtop-vbottom)/(wtop-wbottom);
	A[0][3] = (vleft-wleft)*( (vright-vleft)/(wright-wleft) );
	A[1][3] = (vbottom-wbottom)*( (vtop-vbottom)/(wtop-wbottom) );
}
void initViewMatrix( MATRIX4 A, VECTOR3 eye, VECTOR3 center, VECTOR3 up )
{
	initIdentityMatrix(A);

  VECTOR3 centerMinusEye = initVector3(center.x - eye.x, center.y - eye.y, center.z - eye.z);
  VECTOR3 f = normalizeVector(initVector3( -centerMinusEye.x, -centerMinusEye.y, -centerMinusEye.z));
  VECTOR3 s = normalizeVector( crossProduct(up, f));
  VECTOR3 u = crossProduct(f, s);

  A[0][0] = s.x;
  A[0][1] = s.y;
  A[0][2] = s.z;
  A[0][3] = -dotProduct(s, eye);

  A[1][0] = u.x;
  A[1][1] = u.y;
  A[1][2] = u.z;
  A[1][3] = -dotProduct(u, eye);

  A[2][0] = f.x;
  A[2][1] = f.y;
  A[2][2] = f.z;
  A[2][3] = -dotProduct(f, eye);
}
VECTOR4 mulMatrixVector( MATRIX4 A, VECTOR4 v )
{
	return initVector4(
		A[0][0] * v.x + A[0][1] * v.y + A[0][2] * v.z + A[0][3] * v.w,
		A[1][0] * v.x + A[1][1] * v.y + A[1][2] * v.z + A[1][3] * v.w,
		A[2][0] * v.x + A[2][1] * v.y + A[2][2] * v.z + A[2][3] * v.w,
		A[3][0] * v.x + A[3][1] * v.y + A[3][2] * v.z + A[3][3] * v.w);
}
void mulMatrices( MATRIX4 A, MATRIX4 B, MATRIX4 C )
{
	GLdouble sum;
	for( int i = 0; i < 4; i++)
		for( int j = 0; j < 4; j++) {
			sum = 0;
			for( int k = 0; k < 4; k++)
				sum = sum + A[i][k] * B[k][j];
			C[i][j] = sum;
		}
}

// returns an average vector of four vectors
VECTOR3 getGravity( FACE f )
{
	VECTOR4 gravity = initVector4( (pointsOfTorus[f.v[0]].x + pointsOfTorus[f.v[2]].x ) / 2.0,
	 															 (pointsOfTorus[f.v[0]].y + pointsOfTorus[f.v[2]].y ) / 2.0,
															 	 (pointsOfTorus[f.v[0]].z + pointsOfTorus[f.v[2]].z ) / 2.0,
																 (pointsOfTorus[f.v[0]].w + pointsOfTorus[f.v[2]].w ) / 2.0 );

	return convertToInhomogen( gravity );
}

// ############### COMPARE FUNCTION ##############

bool compareFacesZ( FACE a, FACE b )
{
	// compare the two gravity
	return getGravity(a).z < getGravity(b).z;
}

void init()
{
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// left, right, bottom, top
	glOrtho(0.0f, winWidth, 0.0f, winHeight, 0.0f, 1.0f);

	// camera position z axis
	eye = initVector3( 0.0, 0.0, zoom );
	// the view of the camera origo
	centerVec = initVector3( 0.0, 0.0, 0.0 );
	// up vector for the camera
	up = initVector3( 0.0, 1.0, 0.0 );

	// view matrix
	initViewMatrix(view, eye, centerVec, up);

	initTransformations();
}

void initTransformations()
{
	// projection matrix
	initPersProjMatrix(Vc, center);

  // window to viewport matrix
  double viewLeft = (winWidth - winHeight) / (winHeight / 4);
	// (800 - 600) / (600 / 4)
	// 		200      /    150
	// 					1.3333

	// vleft, vbottom, vright, vtop,
  // wleft, wbottom, wright, wtop
  initWtvMatrix( Wc, viewLeft, 0.0, 600.0, 600.0,
										 -4.0, 	  -4.0, 4.0,   4.0 );

	// Wc_Vc = Wc * Vc
	mulMatrices( Wc, Vc, Wc_Vc );
}

void keyPressed( GLFWwindow * windows, GLint key, GLint scanCode, GLint action, GLint mods )
{
	if( action == GLFW_PRESS || GLFW_REPEAT )
	{
		switch( key )
		{
		case GLFW_KEY_LEFT:
			alpha += deltaAlpha;
			eye.z = cos(alpha) * zoom;
			eye.x = sin(alpha) * zoom;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_RIGHT:
			alpha -= deltaAlpha;
			eye.z = cos(alpha) * zoom;
			eye.x = sin(alpha) * zoom;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_UP:
			upup+=1.0;
			eye.y = upup;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_DOWN:
			upup-=1.0;
			eye.y = upup;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_KP_SUBTRACT:
			zoom += 0.7;
			eye.z = cos(alpha) * zoom;
			eye.x = sin(alpha) * zoom;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_KP_ADD:
			if( zoom  > 0.7 )
				zoom -= 0.7;
			eye.z = cos(alpha) * zoom;
			eye.x = sin(alpha) * zoom;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		}
	}
	glfwPollEvents();
}
